"use strict";

// Теоретичне питання

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Якщо об'єкт B є прототипом об'єкта A, то всякий раз, коли у B є властивість, наприклад колір, A  успадкує той же самий колір, якщо інше не вказано явно.
// Не потрібно повторювати всю інформацію про A, яку він успадковує від  B. A  може успадковуватися від B і B, в свою чергу, може успадковуватися від  C  і т.д.
// Наслідування працює на всій протяжності ланцюжка.Наприклад, якщо  C  вказує колір, але ні B ні A колір не вказують, то він успадкується від C.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// super - це ключове слово, яке використовується для виклику методів батьківського класу в дочірньому класі. Коли ми розширюємо клас за допомогою наслідування, 
// іноді нам потрібно використовувати або модифікувати поведінку батьківського класу, замість того, щоб писати все заново.



// Завдання

// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary
    }
}





class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const ivan = new Programmer("Ivan", 18, 1000, "JS");
console.log(ivan.salary);
console.log(ivan)


const danilo = new Programmer("Danilo", 23, 2000, "Java");
console.log(danilo.salary)
console.log(danilo)
